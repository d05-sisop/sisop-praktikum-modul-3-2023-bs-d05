#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main()
{
	int matriks1[4][2];
	int matriks2[2][5];

	srand(time(NULL));

	//Input Matriks 1
	printf("Matriks 1:\n");
	for (int i=0; i<4; i++){
		for(int j=0; j<2; j++){
			matriks1[i][j] = rand() % 6;
			printf("%d ", matriks1[i][j]);
		}
	printf("\n");
	}

	//Input Matriks 2
	printf("\nMatriks 2:\n");
	for (int i=0; i<2; i++){
		for (int j=0; j<5; j++){
			matriks2[i][j] = rand() % 5;
			printf("%d ", matriks2[i][j]);
		}
	printf("\n");
	}

	//Hitung Perkalian Matriks
	int dp; //dotproduct
	int result[4][5];
	for (int i=0; i<4; i++){
		for (int j=0; j<5; j++){
			for (int k=0; k<2; k++){
				dp = dp + matriks1[i][k] * matriks2[k][j];
			}
		result[i][j]=dp;
		dp = 0;
		}
	}

	//Print Hasil Perkalian
	printf("\nHasil Perkalian Matriks:\n");
	for (int i=0; i<4; i++){
		for (int j=0; j<5; j++){
			printf("%d ", result[i][j]);
		}
	printf("\n");
	}

	//Membuat Shared Memory
	key_t key = 123;
	int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
	int (*arrayresult) [5] = shmat (shmid,NULL,0);

	for (int i=0; i<4; i++){
		for (int j=0; j<5; j++){
			arrayresult[i][j] = result[i][j];
		}
	}

	shmdt((void *) arrayresult);	

	return 0;
}

/*
Reference
https://stackoverflow.com/questions/13589248/generating-random-matrix-in-c
*/
