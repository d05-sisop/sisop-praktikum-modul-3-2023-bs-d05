#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

void factorial(int (*arrayresult)[5], unsigned long long (*result)[5]) {
    // Calculate the factorial of each element in the array
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            unsigned long long fact = 1;
            for (int k = 1; k <= arrayresult[i][j]; k++) {
                fact *= k;
            }
            result[i][j] = fact;
        }
    }
}

int main() {
    key_t key = 123;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");

    unsigned long long result[4][5];
    factorial(arrayresult, result);

    printf("Factorial Array:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }        
    }
    printf ("\n");

    shmdt((void *) arrayresult);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
