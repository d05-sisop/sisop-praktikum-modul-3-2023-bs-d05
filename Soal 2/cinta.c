#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

void *factorial(void *arg) {
    int(*arrayresult)[5] = (int(*)[5])arg;

    unsigned long long result[4][5];
    
    // Calculate the factorial of each element in the array
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            unsigned long long fact = 1;
            for (int k = 1; k <= arrayresult[i][j]; k++) {
                fact *= k;
            }
            result[i][j] = fact; // Cast the result back to int
        }
    }
    printf("Factorial Array:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }        
    }
    printf ("\n");

    pthread_exit(NULL);
}




int main() {
    key_t key = 123;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");

    // factorial
    pthread_t tid [1];
       for (int i = 0; i < 1 ; i++) {
        pthread_create(&tid[i], NULL, &factorial, (void *) arrayresult);
        }

       for (int i = 0; i < 1; i++) {
        pthread_join(tid[i], NULL);
       }
       
    shmdt((void *) arrayresult);
    

    return 0;
}
