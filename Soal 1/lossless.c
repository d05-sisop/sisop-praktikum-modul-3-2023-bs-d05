#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_CHAR 27

// struct huffman tree
struct Node {
    char data;
    unsigned freq;
    struct Node* left;
    struct Node* right;
};

// menghitung frekuensi kemunculan huruf pada file
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Cannot open file: %s\n", filename);
        exit(1);
    }

    int c;
    while ((c = fgetc(file)) != EOF) {
        c = toupper(c); // mengubah ke uppercase
        if (c >= 'A' && c <= 'Z') { 
            freq[c - 'A']++; // menghitung frekuensi karakter alfabet
        }
    }

    fclose(file);
}

// membuat node baru dalam tree
struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}

// menggabungkan dua node yang memiliki frequency paling kecil
struct Node* merge_node(struct Node* left, struct Node* right) {
    struct Node* node = create_node('\0', left->freq + right->freq);
    node->left = left;
    node->right = right;
    return node;
}

// create huffman
struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node* nodes[MAX_CHAR] = { NULL };
    int j = 0;
    for (int i = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }

    int size = j;
    while (size > 1) {
        struct Node* left = nodes[size - 2];
        struct Node* right = nodes[size - 1];
        struct Node* merged = merge_node(left, right);
        nodes[size - 2] = merged;
        size--;
    }

    return nodes[0];
}

void write_huffman_codes(FILE* compressed_file, char* huffCode[MAX_CHAR]) {
    for (int i = 0; i < MAX_CHAR; i++) {
        if (huffCode[i] != NULL) {
            fprintf(compressed_file, "%c:%s\n", i + 'A', huffCode[i]);
        }
    }
}


// fungsi untuk membuat huffman code
void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        if (isalpha(root->data)) {
            code[depth] = '\0';
            huffCode[root->data] = strdup(code);
        }
    }

    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);

    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);

    if (depth > 0) {
        free(huffCode[root->data]);
    }
}

// menyimpan huffman tree ke file yang sudah dicompress
void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data + 'A', file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}

// compress file
void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        printf("Cannot open input file: %s\n", input_filename);
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        printf("Cannot create output file: %s\n", output_filename);
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c)) { // Memeriksa apakah karakter adalah alfabet case insensitive atau spasi
            c = toupper(c); // Mengubah karakter menjadi lowercase
            if (huffCode[c] != NULL) {
                char* code = huffCode[c];
                fwrite(code, sizeof(char), strlen(code), output_file);
            }
        }
    }
    fclose(input_file);
    fclose(output_file);
}

void print_freq(unsigned int freq[MAX_CHAR]) {
    printf("Frequency of each character:\n");
    for (int i = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
                printf("'%c': %u\n", i + 'A', freq[i]);
        }
    }
    printf("\n");
}


int main() {
    const char* input_filename = "file.txt";
    const char* compressed_filename = "compressed_file.txt";

    // hitung frequency karakter
    unsigned int freq[MAX_CHAR] = { 0 };
    count_freq(input_filename, freq);

    print_freq(freq);

    // create huffman tree
    struct Node* root = create_huffman(freq);

    // create pipe
    int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }

    // create child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Fork failed\n");
        exit(1);
    } else if (pid > 0) { //parent process
        close(pipe_fd[1]);

        // frequency setiap karakter dari child process
        unsigned int child_freq[MAX_CHAR];
        read(pipe_fd[0], child_freq, sizeof(child_freq));

        // hitung jumlah bit sebelum file dicompress
        unsigned int original_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += freq[i] * sizeof(char) * 8;
        }
        char* huffCode[MAX_CHAR] = { NULL };
        char code[MAX_CHAR];
        generate_code(root, code, 0, huffCode);

        // simpan huffman tree ke compressed file
        FILE* compressed_file = fopen(compressed_filename, "wb");
        if (compressed_file == NULL) {
            printf("Cannot create compressed file: %s\n", compressed_filename);
            exit(1);
        }
        //save_huffman(root, compressed_file);
        save_huffman(root, compressed_file);
        write_huffman_codes(compressed_file, huffCode);

        fclose(compressed_file);

        compress_file(input_filename, compressed_filename, huffCode);

        // hitung jumlah bit setelah dicompress dengan algoritma huffman
        unsigned int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }

        printf("total bits original file: %u\n", original_bits);
        printf("total bits compressed file: %u\n", compressed_bits);

        // wait child process
        wait(NULL);

    } else {
        close(pipe_fd[0]);

        // write frequency karakter ke parent process
        write(pipe_fd[1], freq, sizeof(freq));
        exit(0);
    }

    return 0;
}


