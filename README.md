# sisop-praktikum-modul-3-2023-BS-D05

# Soal 1
Penjelasan :
- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
- Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Penyelesaian :
1. Membuat 2 pipe
```
int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
```
dan membuat fork
```
pid_t pid = fork();

    if (pid < 0) {
        printf("Fork failed\n");
        exit(1);
    } else if (pid > 0) {
```

2. Selanjutnya pada parent process akan membuat membuka file yang bernama file.txt
```
const char* input_filename = "file.txt";
```

Lalu menghitung frekuensi per karakter yang muncul pada file tersebut.
```
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Cannot open file: %s\n", filename);
        exit(1);
    }

    int c;
    while ((c = fgetc(file)) != EOF) {
        c = toupper(c); 
        if (c >= 'A' && c <= 'Z') { 
            freq[c - 'A']++; 
        }
    }

    fclose(file);
}
```
- Jika `file == NULL` maka file tidak dapat dibuka atau tidak terbaca.
- `while ((c = fgetc(file)) != EOF)` loop akan membaca tiap karakter sampai EOF atau End Of File
- Setiap frekuensi karakter yang muncul akan disimpan dalam array `freq[c]++`
- Karakter yang dibaca hanya alfabet dengan case insensitive, oleh karena itu alfabet diubah menjadi uppercase semua dengan `c = toupper(c);`

3. Mencetak setiap frekuensi dari setiap huruf.
```
void print_freq(unsigned int freq[MAX_CHAR]) {
    printf("Frequency of each character:\n");
    for (int i = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
                printf("'%c': %u\n", i + 'A', freq[i]);
        }
    }
    printf("\n");
}
```

4. Selanjutnya, setiap frekuensi karakter yang muncul akan dibuat Huffman tree

Pertama menginisialisasikan struct tree terlebih dahulu.
```
struct Node {
    char data;
    unsigned freq;
    struct Node *left, *right;
};
```
Lalu membuat node baru untuk tree dengan fungsi create_node
```
struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}
```
- Diinisialisasikan node baru masih kosong `node->left = node->right = NULL;`
- Mengisi data dengan setiap karakter yang akan dibuat tree `node->data = data;`
- Akan mengisi nilai frekuensi dari tiap karakter pada node `node->freq = freq;`

Pada algoritma Huffman, tree akan dibuat berdasarkan frekuensi dari setiap karakter yang muncul, lalu frekuensi tersebut ditambah membentuk simpul hingga tersisa 1 simpul di paling atas tree.

Berikut adalah fungsi untuk membentuk simpul dalam Huffman tree.
```
struct Node* mergeNode(struct Node* left, struct Node* right) {
    struct Node* node = create_node('\0', left->freq + right->freq);
    node->left = left;
    node->right = right;
    return node;
}
```
- `create_node('\0', left->freq + right->freq);` akan membuat node baru dari penggabungan 2 frekuensi dimulai.

Lalu fungsi membuat Huffman tree
```
struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node *nodes[MAX_CHAR];
    int i, j;
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }

    int size = j;
    while (size > 1) {
        struct Node *left = nodes[size - 2];
        struct Node *right = nodes[size - 1];
        struct Node *merged = mergeNode(left, right);
        nodes[size - 2] = merged;
        size--;
    }

    return nodes[0];
}
```
- Loop pertama akan mengiterasi setiap frekuensi karakter, jika frekuensi karakter lebih dari 0 artinya karakter tersebut akan masuk ke dalam tree.
- Loop kedua akan membuat simpul melalui fungsi `merge_node` hingga simpul tersisa 1, jadi loop tersebut akan berjalan selama `(size > 1)`

5. Selanjutnya membuat Huffman Code dari tree yang telah dibuat dari algoritma Huffman.
```
void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        code[depth] = '\0';
        huffCode[root->data] = strdup(code);
    }

    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);

    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);

    if (depth > 0) {
        free(huffCode[root->data]);
    }
}
```
Fungsi tersebut akan membuat Huffman code dengan cara memberikan kode 0 untuk setiap leaf kiri pada tree dan akan memberikan kode 1 untuk setiap leaf kanan. Selama rekursi, string kode akan disimpan secara bertahap dalam `huffCode[root->data] = strdup(code);` sampai mencapai karakter yang dimaksud.

6. Selanjutnya kode yang dibuat akan digunakan untuk mengkompresi file.

Sebelum itu Huffman tree harus disimpan terlebih dahulu menggunakan fungsi `save_huffman`
```
void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data, file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}
```
Fungsi tersebut akan menuliskan kode biner ke dalam file output dan menyimpan struktur dari Huffman tree. Lalu dengan Huffman tree tersebut, file akan dikompresi menggunakan fungsi `compressed_file`
```
void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        printf("Cannot open input file: %s\n", input_filename);
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        printf("Cannot create output file: %s\n", output_filename);
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        char* code = huffCode[c];
        fwrite(code, sizeof(char), strlen(code), output_file);
    }
    fclose(input_file);
    fclose(output_file);
}
```
Fungsi tersebut akan membaca setiap karakter dalam file hingga akhir. Setiap karakter c dalam string huffCode akan disimpan ke dalam variabel code, lalu code akan dituliskan ke dalam file output dengan `strlen(code)`.

7. Menghitung jumlah bits sebelum dilakukan kompresi file dan sesudah kompresi file.

Fungsi untuk menghitung jumlah bits sebelum dikompresi.
```
unsigned long long int original_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += freq[i] * sizeof(char) * 8;
        }
```
Fungsi untuk menghitung jumlah bits pada file yang sudah dikompresi.
```
unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }
```
8. Menampilkan jumlah bits untuk kedua file tersebut.
```
printf("total bits original file: %llu\n", original_bits);
        printf("total bits compressed file: %llu\n", compressed_bits);
```

Output dari program

![nomor 1](Dokumentasi/soal1_lossless.png)

Huffman tree

![huffman tree](Dokumentasi/huffman_tree.png)

# Soal 2

## Penjelasan

### kalian.c

a. Membuat program untuk melakukan perkalian matrix.
Syarat:
Matrix pertama 4x2, berisi angka random 1-5.
Matrix kedua 2x5, berisi angka random 1-4.

### cinta.c
 
b. Membuat program yang mengambil hasil perkalian matrix sebelumnya menggunakan shared memory.

c. Mencari faktorial setiap angka matriks hasil perkalian tersebut menggunakan thread dan multithreading.

### sisop.c
 
d. Lakukan hal sebelumnya tanpa menggunakan thread dan multithreading.

## Penyelesaian

### kalian.c  

1. Membuat dan Input Matrix
```c

    int matriks1[4][2];
    int matriks2[2][5];

    srand(time(NULL));

    //Input Matriks 1
    printf("Matriks 1:\n");
    for (int i=0; i<4; i++){
      for(int j=0; j<2; j++){
        matriks1[i][j] = rand() % 6;
        printf("%d ", matriks1[i][j]);
      }
    printf("\n");
    }

    //Input Matriks 2
    printf("\nMatriks 2:\n");
    for (int i=0; i<2; i++){
      for (int j=0; j<5; j++){
        matriks2[i][j] = rand() % 5;
        printf("%d ", matriks2[i][j]);
      }
    printf("\n");
    }
  
```

- Membuat array untuk matrix 1 dan 2 sesuai ukuran pada soal.
- `srand(time(NULL))` untuk seed random number dari fungsi time.h
- `rand()` untuk mengenerate random number.
- Menggunakan modulo untuk membatasi angka sesuai permintaan soal yaitu 1-5 dan 1-4.

2. Perkalian Matrix
```c
 
    int dp; //dotproduct
    int result[4][5];
    for (int i=0; i<4; i++){
      for (int j=0; j<5; j++){
        for (int k=0; k<2; k++){
          dp = dp + matriks1[i][k] * matriks2[k][j];
        }
      result[i][j]=dp;
      dp = 0;
      }
    }

```

- Membuat variabel dp untuk menyimpan dotproduct dan result untuk menyimpan hasil.
- Melakukan perkalian sesuai rumus matrix menggunakan nested loop.
- Mengalikan tiap elemen lalu menambahkan ke `dp` sehingga dot product telah terhitung.
- Memasukkan hasil peralian baris dan kolom (dot product) kedalam result.
- `dp = 0` reset nilai dp untuk menghitung baris dan kolom berikutnya. 

3. Menampilkan Hasil
```c
    
    printf("\nHasil Perkalian Matriks:\n");
    for (int i=0; i<4; i++){
      for (int j=0; j<5; j++){
        printf("%d ", result[i][j]);
      }
    printf("\n");
    }
    
```

- Print hasil matrix yang telah disimpan di `result`
- Menggunakan perulangan for sebanyak ukuran matrix hasil yaitu 4x5. 

4. Membuat Shared Memory
```c

    key_t key = 123;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    for (int i=0; i<4; i++){
      for (int j=0; j<5; j++){
        arrayresult[i][j] = result[i][j];
      }
    }

    shmdt((void *) arrayresult);
    
```

- Membuat key dan inisialisasi shared memory.
- Membuat pointer arrayresult yang akan menyimpan nilai yang dishare.
- Memasukkan `result` yang akan digunakan shared memory kedalam `arrayresult`. 

### cinta.c

1. Fungsi Faktorial
```c

    void *factorial(void *arg) {
        int(*arrayresult)[5] = (int(*)[5])arg;

        unsigned long long result[4][5];

        // Calculate the factorial of each element in the array
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                unsigned long long fact = 1;
                for (int k = 1; k <= arrayresult[i][j]; k++) {
                    fact *= k;
                }
                result[i][j] = fact; // Cast the result back to int
            }
        }
        printf("Factorial Array:\n");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                printf("%llu ", result[i][j]);
            }        
        }
        printf ("\n");

        pthread_exit(NULL);
    }
    
```

- Fungsi untuk menghitung faktorial matrix.
- Membuat variabel result sebagai output dari fungsi.
- `unsigned long long fact = 1` sebagai default nilai karena 0 jika difaktorial adalah 1.
- `fact *= k` akan melakukan perkalian fact dengan k lalu memasukkan hasil kedalam fact.
- Setelah perulangan selesai menghitung urut perkalian hasil akan dimasukkan ke dalam `result`.
- Lalu print hasil faktorial.

2. Shared Memory
```c

    key_t key = 123;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");

```

- Inisialilasi dan menggunakan shared memory.
- Mengambil nilai `arrayresult` pada shared memory yang telah dimasukkan sebelumnya.
- Lalu print nilai yang diambil untuk mengecek.

3. Memanggil Thread
```c

    pthread_t tid [1];
    pthread_create(&tid[i], NULL, &factorial, (void *) arrayresult);
    pthread_join(tid[i], NULL);

```

- Menginisialisasi penggunaan thread sebanyak 1.
- Create thread tersebut dengan argumen fungsi factorial yang telah dibuat.
- Parameter thread adalah `arrayresult` dari shared memory.
- `pthread_join` untuk menunggu thread sebelum melanjutkan.

### sisop.c

1. Fungsi Faktorial

```c
    void factorial(int (*arrayresult)[5], unsigned long long (*result)[5]) {
        // Calculate the factorial of each element in the array
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                unsigned long long fact = 1;
                for (int k = 1; k <= arrayresult[i][j]; k++) {
                    fact *= k;
                }
                result[i][j] = fact;
            }
        }
    }

```

- Fungsi faktorial untuk menghitung faktorial dari matrix.
- `unsigned long long fact = 1` sebagai default nilai karena 0 jika difaktorial adalah 1.
- `fact *= k` akan melakukan perkalian fact dengan k lalu memasukkan hasil kedalam fact.
- Setelah perulangan selesai menghitung urut perkalian hasil akan dimasukkan ke dalam `result`.

2. Memanggil Shared Memory

```c

    key_t key = 123;
    int shmid = shmget(key, 4*5*sizeof(int), IPC_CREAT | 0666);
    int (*arrayresult) [5] = shmat (shmid,NULL,0);

    printf ("hasil dari kalian.c\n");
    
    for (int i=0; i<4 ; i++){
        for (int j = 0 ; j<5 ; j++){
            printf("%d ", arrayresult[i][j]);
        }
        printf ("\n");
    }
    printf ("\n");

```

- Inisialilasi dan menggunakan shared memory.
- Mengambil nilai `arrayresult` pada shared memory yang telah dimasukkan sebelumnya.
- Lalu print nilai yang diambil untuk mengecek.

3. Memanggil Fungsi dan Print Hasil

```c

    unsigned long long result[4][5];
    factorial(arrayresult, result);

    printf("Factorial Array:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }        
    }
    printf ("\n");

```

- Membuat variabel result untuk menyimpan hasil faktorial.
- Memanggil fungsi factorial yang telah dibuat dengan input nilai matrix dari `arrayresult`.
- Print hasil perhitungan faktorial yang disimpan di array `result`.

## Output Program

### kalian.c
![nomor 2](Dokumentasi/soal2_kalian.png)

### cinta.c
![nomor 2](Dokumentasi/soal2_cinta.png)

### sisop.c
![nomor 2](Dokumentasi/soal2_sisop.png)

# Soal 3
Membuat sistem untuk melakukan stream lagu. 

a) Membuat sistem stream sebagai receiver (stream.c) dan user dengan menggunakan message queue. User hanya  dapat mengirimkan perintah berupa string dan semua aktivitas sesuai perintah akan dilaksanakan oleh sistem.

b) User dapat mengirimkan perintah DECRYPT dan sistem stream akan melakukan decript pada file song-playlist.json

*sistem
`void decryptSongName(char *songName) {
    int len = strlen(songName);
    for (int i = 0; i < len; i++) {
        if (isalpha(songName[i])) {
            if (islower(songName[i]))
                songName[i] = 'z' - (songName[i] - 'a');
            else if (isupper(songName[i]))
                songName[i] = 'Z' - (songName[i] - 'A');
        }
    }
}`

dalam fungsi main:

`// Perintah untuk decrypt song-playlist.json dan membuat playlist.txt
        if (strcmp(msg.text, "DECRYPT") == 0) {
            createPlaylist();
        }`

*user
`// Mengirim perintah DECRYPT ke sistem stream
    strcpy(msg.text, "DECRYPT");
    msg.type = MSG_TYPE;
    if (msgsnd(msgid, &msg, sizeof(msg.text), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }`

### Penjelasan:
1.	void decryptSongName(char *songName): Mendefinisikan fungsi decryptSongName yang mengambil parameter berupa pointer ke karakter (char *songName) dan tidak mengembalikan nilai (void).
2.	int len = strlen(songName);: Menghitung panjang string songName menggunakan fungsi strlen() dan menyimpan hasilnya dalam variabel len. Panjang ini akan digunakan untuk melakukan iterasi melalui setiap karakter dalam string.
3.	for (int i = 0; i < len; i++) {: Memulai loop untuk mengiterasi melalui setiap karakter dalam songName, mulai dari indeks 0 hingga len-1.
4.	if (isalpha(songName[i])) {: Memeriksa apakah karakter pada indeks i dalam songName merupakan karakter alfabet menggunakan fungsi isalpha(). Ini digunakan untuk memastikan bahwa hanya karakter alfabet yang akan diubah.
5.	if (islower(songName[i])): Memeriksa apakah karakter pada indeks i dalam songName merupakan huruf kecil menggunakan fungsi islower().
a. Jika iya, maka karakter tersebut diubah menjadi karakter yang sesuai dengan aturan dekripsi. Karakter tersebut akan diubah menjadi karakter yang terletak di posisi yang berlawanan di dalam abjad kecil. Misalnya, karakter 'a' akan diubah menjadi 'z', 'b' menjadi 'y', dan seterusnya. Hal ini dilakukan dengan mengurangi nilai ASCII dari karakter tersebut dengan nilai ASCII dari 'a', kemudian mengurangi hasilnya dari karakter 'z' yang memiliki nilai ASCII yang lebih besar dari 'a'.


c) User dapat mengrimkan perintah LIST, sistem akan menampilkan daftar lagu yang telah yang telah di DECRYPT

*sistem
`// Fungsi untuk menampilkan daftar lagu yang telah di-decrypt
void displayPlaylist() {
    FILE *playlistFile = fopen("playlist.txt", "r");

    if (playlistFile == NULL) {
        perror("File error");
        exit(1);
    }

    char song[256];
    int count = 1;

    printf("Daftar lagu:\n");`
*user
`// Mengirim perintah LIST ke sistem stream
    strcpy(msg.text, "LIST");
    msg.type = MSG_TYPE;
    if (msgsnd(msgid, &msg, sizeof(msg.text), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }`

dalam fungsi main:
`// Perintah untuk menampilkan daftar lagu yang telah di-decrypt
        else if (strcmp(msg.text, "LIST") == 0) {
            displayPlaylist();
        }`

###Penjelasan:
1.	void playSong(char *songName, int userId): Mendefinisikan fungsi playSong yang mengambil dua parameter, yaitu songName yang merupakan pointer ke string yang berisi nama lagu, dan userId yang merupakan ID pengguna yang memainkan lagu tersebut. Fungsi ini tidak mengembalikan nilai (void).
2.	decryptSongName(songName);: Memanggil fungsi decryptSongName() yang telah didefinisikan sebelumnya untuk melakukan dekripsi atau konversi pada nama lagu yang ada dalam songName. Fungsi ini akan mengubah karakter-karakter alfabet dalam nama lagu menjadi karakter yang sesuai dengan aturan dekripsi.
3.	printf("USER %d PLAYING \"%s\"\n", userId, songName);: Mencetak pesan ke layar dengan menggunakan printf(). Pesan tersebut menyatakan bahwa pengguna dengan ID userId sedang memainkan lagu dengan nama songName. Format penulisan menggunakan spesifikasi format %d untuk memasukkan nilai userId dan %s untuk memasukkan string songName. Nama lagu ditempatkan di dalam tanda kutip ganda untuk menunjukkan bahwa itu adalah nama lagu.


d) User dapat  mengirimkan perintah PLAY <SONG> dengan ketentuan: (contoh: USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART")

*sistem
`// Fungsi untuk memainkan lagu
void playSong(char *songName, int userId) {
    decryptSongName(songName);
    printf("USER %d PLAYING \"%s\"\n", userId, songName);
}`
*user
`// Mengirim perintah PLAY "Stereo Heart" ke sistem stream
    strcpy(msg.text, "PLAY \"Stereo Heart\"");
    msg.type = MSG_TYPE;
    if (msgsnd(msgid, &msg, sizeof(msg.text), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }`

fungsi dalam main:

`// Perintah untuk memainkan lagu
        else if (strncmp(msg.text, "PLAY", 4) == 0) {
            char songName[MAX_MSG_SIZE - 5];
            strcpy(songName, msg.text + 5);
            playSong(songName, msg.type);
        }`



e) User dapat menambahkan lagu ke dalam playlist dengan syarat:

*User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

*User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

*sistem
`/ Fungsi untuk menambahkan lagu ke dalam file song-playlist.json
void addSong(const char *songName) {
    FILE *jsonFile = fopen("song-playlist.json", "a");

    if (jsonFile == NULL) {
        perror("File error");
        exit(1);
    }

    fprintf(jsonFile, "%s\n", songName);

    fclose(jsonFile);

    printf("Lagu \"%s\" berhasil ditambahkan.\n", songName);
}`
*user
`// Mengirim perintah ADD "My Song" ke sistem stream
    strcpy(msg.text, "ADD \"My Song\"");
    msg.type = MSG_TYPE;
    if (msgsnd(msgid, &msg, sizeof(msg.text), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }`

fungsi dalam main:
` // Perintah untuk menambahkan lagu
    else if (strncmp(msg.text, "ADD", 3) == 0) {
        char songName[MAX_MSG_SIZE - 4];
        strcpy(songName, msg.text + 4);
        addSong(songName);`

1.	void addSong(const char *songName): Mendefinisikan fungsi addSong yang mengambil satu parameter, yaitu songName yang merupakan pointer ke string yang berisi nama lagu yang akan ditambahkan. Fungsi ini tidak mengembalikan nilai (void).
2.	FILE *jsonFile = fopen("song-playlist.json", "a");: Membuka file song-playlist.json dalam mode "append" menggunakan fungsi fopen(). Mode "append" ("a") digunakan untuk menambahkan data ke akhir file. Fungsi ini mengembalikan pointer ke file (FILE *) yang akan digunakan untuk menulis data ke dalam file JSON.
3.	if (jsonFile == NULL) { ... }: Memeriksa apakah pembukaan file song-playlist.json gagal. Jika file tidak dapat dibuka (nilai pointer jsonFile adalah NULL), maka pesan kesalahan akan dicetak menggunakan perror() dan program akan dihentikan menggunakan exit(1).
4.	fprintf(jsonFile, "%s\n", songName);: Menulis nama lagu (songName) ke dalam file JSON menggunakan fungsi fprintf(). Format penulisan adalah string songName diikuti oleh karakter baris baru (\n), sehingga setiap lagu ditulis pada baris yang baru.
5.	fclose(jsonFile);: Menutup file JSON yang telah ditulis menggunakan fungsi fclose() untuk melepaskan sumber daya yang digunakan oleh file.
6.	printf("Lagu \"%s\" berhasil ditambahkan.\n", songName);: Mencetak pesan ke layar menggunakan printf(). Pesan tersebut menyatakan bahwa lagu dengan nama songName berhasil ditambahkan ke dalam file. Nama lagu ditempatkan di dalam tanda kutip ganda untuk menunjukkan bahwa itu adalah nama lagu.

f) Menggunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
meng-Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

g) Menampilkan "UNKNOWN COMMAND" apabila perintah tidak dipahami oleh sistem

*sistem
`// Perintah tidak dikenali
        else {
            printf("UNKNOWN COMMAND\n");
        }`
*user
`// Mengirim perintah UNKNOWN COMMAND ke sistem stream
    strcpy(msg.text, "UNKNOWN COMMAND");
    msg.type = MSG_TYPE;
    if (msgsnd(msgid, &msg, sizeof(msg.text), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }`







